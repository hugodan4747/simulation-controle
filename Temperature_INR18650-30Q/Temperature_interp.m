clc;
clear all;
close all; 

% Importation des donn�es
Temp_1C = [table2array(readtable('Temp_1C.csv'))];
Temp_5A = [table2array(readtable('Temp_5A.csv'))];
Temp_10A = [table2array(readtable('Temp_10A.csv'))];
Temp_15A = [table2array(readtable('Temp_15A.csv'))];
Temp_20A = [table2array(readtable('Temp_20A.csv'))];

%%

XX = linspace(0,max(Temp_20A(:,1)),100);
YY_1C = interp1([0;Temp_1C(:,1)],[23;Temp_1C(:,2)],XX);
YY_5A = interp1([0;Temp_5A(:,1)],[23;Temp_5A(:,2)],XX);
YY_10A = interp1([0;Temp_10A(:,1)],[23;Temp_10A(:,2)],XX);
YY_15A = interp1([0;Temp_15A(:,1)],[23;Temp_15A(:,2)],XX);
YY_20A = interp1([0;Temp_20A(:,1)],[23;Temp_20A(:,2)],XX);


figure
hold on
plot(Temp_1C(:,1),Temp_1C(:,2),'r')
plot(XX,YY_1C)
plot(XX,YY_5A)
plot(XX,YY_10A)
plot(XX,YY_15A)
plot(XX,YY_20A)
hold off

filename = 'Temp_XX';
save(filename,'XX')
filename = 'Temp_1C';
save(filename,'YY_1C')
filename = 'Temp_5A';
save(filename,'YY_5A')
filename = 'Temp_10A';
save(filename,'YY_10A')
filename = 'Temp_15A';
save(filename,'YY_15A')
filename = 'Temp_20A';
save(filename,'YY_20A')


