clc;
clear all;
close all; 
%%

% Importation des donn�es
csv_0_86_0_90 = table2array(readtable('efficiency_map_raw_data/0_86_0_90.csv')) ;
csv_0_90_0_94 = table2array(readtable('efficiency_map_raw_data/0_90_0_94.csv')) ;
csv_094 = table2array(readtable('efficiency_map_raw_data/0_94.csv')) ;
csv_096 = table2array(readtable('efficiency_map_raw_data/0_96.csv')) ;
csv_095 = table2array(readtable('efficiency_map_raw_data/0_95.csv')) ; 

%%
len = 251;
half_len = 50;



for a = half_len:-1:1 
    for row = 1:1:len
        for col = 1:1:len        
            if (row  == a) || (col == a)
                D(row,col) = a/half_len;
            end
            if (row  == len-a+1) || (col == len-a+1)
                D(row,col) = a/half_len;
            end
        end
    end   
end

for row = 1:1:len
    for col = 1:1:len        
        if D(row,col) == 0
            D(row,col) = 1;
        end
    end
end   

    
%% Cr�ation de la table de r�f�rence vierge
    
n_torque = 250 ;
n_speed = 5000 ;
torque   = 0:1:n_torque ;    % Couple de la machine � induction [Nm]
speed    = 0:(n_speed+1)/length(torque):n_speed ;       % Vitesse de rotation de la machine � induction [rpm]
efficacite_min = 0.85;
eff_map  = efficacite_min * ones(length(torque), length(speed));
eff_map1 = D.*ones(length(torque), length(speed));
    
%% Creation des courbes de niveau

x_96 = csv_096(1:end,1) ;
y_96 = csv_096(1:end,2) ;
x_95 = csv_095(1:end,1) ;
y_95 = csv_095(1:end,2) ;
x_94 = csv_094(1:end,1) ;
y_94 = csv_094(1:end,2) ;
x_90 = csv_0_90_0_94(1:end,1) ;
y_90 = csv_0_90_0_94(1:end,2) ;
x_86 = csv_0_86_0_90(1:end,1) ;
y_86 = csv_0_86_0_90(1:end,2) ;

k_96 = boundary(x_96,y_96);
k_95 = boundary(x_95,y_95);
k_94 = boundary(x_94,y_94);
k_90 = boundary(x_90,y_90);
k_86 = boundary(x_86,y_86);

% figure;
% hold on;
% plot(x_96(k_96),y_96(k_96));
% plot(x_95(k_95),y_95(k_95));
% plot(x_94(k_94),y_94(k_94));
% plot(x_90(k_90),y_90(k_90));
% plot(x_86(k_86),y_86(k_86));
% hold off;

%% Transposition des courbes ne niveau en points de meshgrid
x0 = min(speed) ; x1 = max(speed) ;
y0 = min(torque) ; y1 = max(torque) ;
[X,Y] = meshgrid(linspace(x0,x1),linspace(y0,y1)) ;
id_96 = inpolygon(X(:),Y(:),x_96(k_96),y_96(k_96)) ;
id_95 = inpolygon(X(:),Y(:),x_95(k_95),y_95(k_95)) ;
id_94 = inpolygon(X(:),Y(:),x_94(k_94),y_94(k_94)) ;
id_90 = inpolygon(X(:),Y(:),x_90(k_90),y_90(k_90)) ;
id_86 = inpolygon(X(:),Y(:),x_86(k_86),y_86(k_86)) ;
% X(~idx) = NaN ; 
% Y(~idx) = NaN ;
% plot(x_96,y_96,'.r')
% plot(x_95,y_95,'.r')
% plot(x_94,y_94,'.r')
% plot(x_90,y_90,'.r')
% plot(x_86,y_86,'.r')
% hold on
% 
% plot(X(id_86),Y(id_86),'.r')
% plot(X(id_90),Y(id_90),'.g')
% plot(X(id_94),Y(id_94),'.y')
% plot(X(id_95),Y(id_95),'.b')
% plot(X(id_96),Y(id_96),'.m')
% plot(X(~id_86),Y(~id_86),'.k')
% hold off

%% Creation de l'effiency map finale

eff_map_228_resize = griddata(speed,torque,eff_map,X,Y);
eff_map1_228_resize = griddata(speed,torque,eff_map1,X,Y);

for i = 1:length(id_86)
    eff_map_228_resize(id_86) = 0.88;
end
for i = 1:length(id_90)
    eff_map_228_resize(id_90) = 0.92;
end
for i = 1:length(id_94)
    eff_map_228_resize(id_94) = 0.94;
end
for i = 1:length(id_95)
    eff_map_228_resize(id_95) = 0.95;
end
for i = 1:length(id_96)
    eff_map_228_resize(id_96) = 0.96;
end



for i = 1:length(id_86)
    eff_map1_228_resize(id_86) = 0.88;
end
for i = 1:length(id_90)
    eff_map1_228_resize(id_90) = 0.92;
end
for i = 1:length(id_94)
    eff_map1_228_resize(id_94) = 0.94;
end
for i = 1:length(id_95)
    eff_map1_228_resize(id_95) = 0.95;
end
for i = 1:length(id_96)
    eff_map1_228_resize(id_96) = 0.96;
end

     
%% Cartographie d'efficacit�

figure;
[C,h] = contourf(X,Y,eff_map_228_resize);
clabel(C,h);
colorbar;
xlabel('Vitesse (rpm)');
ylabel('Couple (N*m)');
title('Cartographie d''efficacite');

figure;
[C,h] = contourf(X,Y,eff_map1_228_resize);
clabel(C,h);
colorbar;
xlabel('Vitesse (rpm)');
ylabel('Couple (N*m)');
title('Cartographie d''efficacite test');
    
%% EMRAX 268

Tmax_228 = 240;
Tmax_268 = 500;
Tfactor = Tmax_268/Tmax_228;

torque_   = linspace(0,Tmax_268,209);    % Couple de la machine � induction [Nm]

x0_ = min(speed) ; x1_ = max(speed) ;
y0_ = min(torque_) ; y1_ = max(torque_) ;
[X_,Y_] = meshgrid(linspace(x0_,x1_),linspace(y0_,y1_)) ;

Xscaled = imresize(X_,Tfactor,'bilinear');
Yscaled = imresize(Y_,Tfactor,'bilinear');
eff_map_268 = imresize(eff_map1_228_resize,Tfactor,'bilinear');

figure;
[C,h] = contourf(Xscaled,Yscaled,eff_map_268);
clabel(C,h);
colorbar;
xlabel('Vitesse (rpm)');
ylabel('Couple (N*m)');
title('Cartographie d''efficacite du moteur EMRAX268');

Speed_vec = Xscaled(1,1:end-1);
Torque_vec = Yscaled(1:end-1,1);
Torque_vec = Torque_vec';
Efficiency_Map_EMRAX268 = eff_map_268(1:end-1,1:end-1);
filename = 'Torque_vec';
save(filename,'Torque_vec')
filename = 'Speed_vec';
save(filename,'Speed_vec')
filename = 'Efficiency_Map_EMRAX268';
save(filename,'Efficiency_Map_EMRAX268')


