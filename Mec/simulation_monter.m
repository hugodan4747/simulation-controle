function [ P ] = simulation_monter( P )
% Détermination de la puissance au décollage
%  Permet de calculer la puissance au décollage ainsi que le temps que cela
%  a pris et l'énergie total consommé durant cette période. 
phi=asin(P.taux_monte/P.V_monte_takeoff);           % rad angle of climb
rho=P.rho_0;
%% Forces en présence
W=P.m.gross*9.81;                      % N poid
Cl=W*cos(phi)./(0.5*rho*P.V_monte_takeoff.^2*P.S);  % N Lift (coeff)

Cl(Cl>1.5)=NaN;
Cd=P.Cd0+Cl.^2/(pi*P.AR*0.85);         % N Drag (coeff)
D=Cd*0.5*rho.*P.V_monte_takeoff.^2*P.S;             % N Drag 
T=D+W*sin(phi);                        % N Thrust 

%% Détermination de la puissance
Puissance=T.*P.V_monte_takeoff./P.n_helice_take_off/P.n_batterie;
P.Puissance_kW.monter=Puissance/1000;
P.T.monter=T;
P.Cl.monter=Cl;
%% Détermination du temps de monté et de l'énergie nécéssaire
P.delta_t.monter=P.Altitude_cruise./P.taux_monte;
P.Energie_kWh.monter=P.delta_t.monter/3600.*P.Puissance_kW.monter;
P.thrust.monter=T;
end

