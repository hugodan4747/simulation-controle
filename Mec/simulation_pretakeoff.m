function [ P ] = simulation_pretakeoff(P)
% D�termine l'�nergie prise tout juste avant le d�collage
%  permet de d�terminer l'�nergie requise au d�collage

 P.Puissance_kW.pretakeoff=P.Power_decollage_w/1000;

P.Energie_kWh.pretakeoff=P.Power_decollage_w/1000*P.delta_t.pretakeoff/3600; % kwh �nergie pour partir
P.takeoff.acceleration=P.V_monte_takeoff/P.delta_t.pretakeoff;  % kwh �nergie pour partir
%0.9*P.n_helice*P.Power_max_w/P.V_monte_takeoff/P.m.gross; 
P.takeoff.Dist_m_takeoff=P.V_monte_takeoff^2/2/P.takeoff.acceleration;
end

