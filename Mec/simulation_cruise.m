function [P ] = simulation_cruise( P)
% Simulation des performance en vol stationnaire
%   Detailed explanation goes here
% (https://www.deleze.name/marcel/sec2/applmaths/pression-altitude/masse_volumique.pdf)
rho=352.995.*(1 - 0.0000225577.*P.Altitude_cruise).^5.25516./(288.15 - 0.0065.*P.Altitude_cruise);

%% Évaluation des performances
%% Choix du mode de calcul des performances (vitesse unitaire ou optimisable avec un vecteur)
switch P.vitesse_optimise
    case 'off'
        v=P.v_cruise;          % m/s    Vitesse
    case 'on'
        v=linspace(5,100,300); % m/s    Vitesse
end
%%

clc
% Évaluation de la puissance et du ratio Cl/Cd
% Coeff de portance
Cl=P.m.gross.*9.81./(0.5.*rho.*v.^2.*P.S)
% Coeff de drag (en fonction du coeff de portance)
Cd=P.Cd0+Cl.^2./(pi.*P.AR.*0.85)
thrust=Cd.*0.5.*rho.*v.^2.*P.S

% Calcul de puissance = Thrust(=drag).*vitesse.*facteur de rendement
Puissance=thrust.*v./P.n_helice./P.n_batterie
% Calcul de la finesse
ClsurCd=Cl./Cd

switch P.vitesse_optimise
    case 'off'
        Puissance_kW_cruise=Puissance./1000
        P.thrust.cruise=thrust;
        P.Puissance_kW.cruise=Puissance_kW_cruise;
        P.Energie_kWh.cruise=P.Energie_kWh.total-P.Energie_kWh.pretakeoff-P.Energie_kWh.monter;
        
        P.delta_t.cruise=P.Energie_kWh.cruise./P.Puissance_kW.cruise.*3600;
        P.finesse.cruise=ClsurCd;
        P.Cl.cruise=Cl;
    case 'on'
        
       % Détermination de la vitesse au point de finesse max
        [P.finesse.cruise,i]=max(ClsurCd);
        P.v_cruise=v(i);

        Cd=Cd(i)
        Cl=Cl(i)
        P.Cl.cruise=Cl;
        % Détermination de la puissance au point de finesse
        P.Puissance_kW.cruise=Puissance(i)/1000;
        
        % Détermination de la quantité d'énergie
        P.Energie_kWh.cruise=P.Energie_kWh.total-(P.Energie_kWh.pretakeoff+2*P.Energie_kWh.monter);
        % Détermination du temps de consommation en seconde
        P.delta_t.cruise=P.Energie_kWh.cruise./P.Puissance_kW.cruise.*3600;
        % Détermination de la poussé en cruise
        P.thrust.cruise=thrust(i);
        P.range_km= P.delta_t.cruise * v(i);
        
       
end

end

