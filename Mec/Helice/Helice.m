clc;
clear all;
close all; 
%%

% Importation des donn�es
D1660_B20 = table2array(readtable('DUC_1660mm_beta20.txt')) ;
D1660_B23 = table2array(readtable('DUC_1660mm_beta23.txt')) ;
D1660_B16 = table2array(readtable('DUC_1660mm_beta16.txt')) ;

%% Inputs
RPM_max = 3500; %rpm
RPM_min = 1000; %rpm
delta_rpm = 50;
rho_0 = 0.002377; %slug/ft�, air density at sea level
rho_2000 = 0.002245; %slug/ft�, air density at 2000 ft
rho_5000 = 0.002048; %slug/ft�, air density at 5000 ft
rho = rho_0;
sound_speed = 1125; %ft/sec

%%
H.J = D1660_B20(:,1);  %initialization of advance ratio
H.eta = D1660_B20(:,2);  %initialization of efficiency
H.CT = D1660_B20(:,3);  %initialization of thrust coefficient
H.CP = D1660_B20(:,4);  %initialization of power coefficient
dia = 65.35/12; %ft

H.CTJ2 = H.CT./H.J.^2;
H.CTJ2(1,1) = 9999; % replace inf to make proper interpolationdata

H.J_flip = fliplr(H.J');
H.J_flip = H.J_flip';
H.CTJ2_flip = fliplr(H.CTJ2');
H.CTJ2_flip = H.CTJ2_flip';

filename = 'H';
save(filename,'H');

% TAS = zeros(length(J),(RPM_max-RPM_min)/delta_rpm+1);
% T = zeros(length(J),(RPM_max-RPM_min)/delta_rpm+1);
% P = zeros(length(J),(RPM_max-RPM_min)/delta_rpm+1);
% C = zeros(length(J),(RPM_max-RPM_min)/delta_rpm+1);
% 
% rpm = RPM_min:delta_rpm:RPM_max;
% rps = rpm./60;
% TAS = J.*rps*dia*0.681818; %mph (ft/s -> mph = 0.681818)
% 
% T = CT*rho.*rps.^2*dia^4*4.44822; %thrust, Newtons (lb -> Newton = 4.44822)
% P = CP*rho.*rps.^3*dia^5*0.001818*0.7457; %power, kW (lb*ft/sec -> hp -> kW)   
% C = P*1000./(rpm*2*pi/60);  %torque, Nm
% 
% %%
% figure()
% surf(rpm, TAS, C)
% %title(strcat('Torque VS RPM and TAS (pitch = ',num2str(pitch) ,'�)'))
% zlabel('Torque (Nm)')
% ylabel('TAS (mph)')
% xlabel('RPM')
% figure()
% surf(rpm, TAS, T)
% %title(strcat('Thrust VS RPM and TAS (pitch = ',num2str(pitch) ,'�)'))
% zlabel('Thrust (N)')
% ylabel('TAS (mph)')
% xlabel('RPM')
% figure()
% surf(rpm, TAS, P)
% %title(strcat('Power VS RPM and TAS (pitch = ',num2str(pitch) ,'�)'))
% zlabel('Power (kW)')
% ylabel('TAS (mph)')
% xlabel('RPM')