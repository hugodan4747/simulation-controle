function [minClosestValue,position] = findMinClosestValue(Value, matrix)

[~,position] = min(abs(matrix-Value));
minClosestValue = matrix(position);

end

