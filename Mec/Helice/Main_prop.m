clc;
clear all;
close all; 

%% Input all prop file for different pitch angles here
prop = 65;
if prop == 60
% 60 (1520 mm) inches DUC prop 
file_list = {'DUC_1520mm_beta25.txt';'DUC_1520mm_beta26.txt';...
    'DUC_1520mm_beta27.txt';'DUC_1520mm_beta28.txt';'DUC_1520mm_beta29.txt';...
    'DUC_1520mm_beta31.txt';'DUC_1520mm_beta32.txt'};
pitch_list = [25, 26, 27, 28, 29, 31, 32];
dia = 59.84252/12; %ft

elseif prop == 65
% 65 (1660 mm) inches DUC prop
file_list = {'DUC_1660mm_beta16.txt';'DUC_1660mm_beta20.txt';'DUC_1660mm_beta23.txt'};
pitch_list = [16, 20, 23];
dia = 65.35/12; %ft

elseif prop == 68
% % 68 (1730 mm) inches DUC prop
file_list = {'DUC_1730mm_beta5.txt','DUC_1730mm_beta8.txt','DUC_1730mm_beta11.txt',...
         'DUC_1730mm_beta14.txt','DUC_1730mm_beta17.txt','DUC_1730mm_beta20.txt',...
         'DUC_1730mm_beta23.txt','DUC_1730mm_beta26.txt','DUC_1730mm_beta29.txt',...
         'DUC_1730mm_beta32.txt'}; 
pitch_list = [5, 8, 11, 14, 17, 20, 23, 26, 29, 32];
dia = 68.11/12; %ft 
end
%% Run Perfos_prop for all pitch angles
time_temp = 0;
pitch_best = 0;
for i = 1:length(file_list)
    file = char(file_list(i));
    pitch = pitch_list(i);
    run('Perfos_prop.m')
    if time_cruise > time_temp %Determine best operating pitch for a given propeller based on cruise autonomy
        time_temp = time_cruise;
        distance = TAS_cruise*1.60934*time_temp/60; %mph to km/h = 1.60934 and min to hour  = 1/60
        pitch_best = pitch;   
    end
end
%% Display best pitch configuration
if pitch_best ~= 0 %means that a propeller configuration allows to get the required performances
    fprintf('For this propeller, the best configuration is for a %0.0f� pitch where the cruise autonomy is %0.2f min (%0.2f km)\n', pitch_best, time_temp, distance)
else
    fprintf('There is no propeller configuration that can meet the performances with the actual contraints')
end




