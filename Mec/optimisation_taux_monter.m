function [taux_monte_optimal,vitesse_avance_monte_optimal] = optimisation_taux_monter(P, intervalle_TAS_mph, intervalle_taux_monte_fpm)
%optimisation_taux_monter : Est une fonction qui �value les performances de
%d�collage en fonction des contraintes de puissance et de force maximale de
%l'avion. Trouvant les param�tres optimale respectant les contraintes
%
%   Donn�es optimisable : 
% ---------------------------------
% Vitesse avance (True air speed)
% Vitesse de mont�
% 
%   Contraintes : 
% ---------------------------------
% - Force max h�lice (Approximativement constante pour des vitesse faible
% - Power max du syst�me

%% 
switch P.vitesse_optimise
    case 'on'
    % Cr�ation des vecteurs de possibilit�s
    V_avance_en_monter=convvel(intervalle_TAS_mph,'mph','m/s');     % m/s vitesse d'avance
    taux_monter= convvel(intervalle_taux_monte_fpm,'ft/min','m/s');         % m/s taux de mont�e
    Thrust_max=P.Thrust_max_N; % N
    Power_max=P.puissance_moteur_max/1000; % kW
    
    for  i=1:length(V_avance_en_monter)
        P.V_monte_takeoff =V_avance_en_monter(i);
        P.taux_monte= taux_monter;         % m/s taux de mont�e
        [P] = simulation_monter( P);
        
          % Trouver le taux de mont� max selon le crit�re de thrust
        taux_monte_optimal_thrust(i)=spline(P.T.monter,P.taux_monte,Thrust_max);
          % Trouver le taux de mont� max selon le crit�re de Power
        taux_monte_optimal_power(i)=spline(P.Puissance_kW.monter,P.taux_monte,Power_max);
          % Trouver le taux de mont� minimum entre les deux crit�res
        taux_monte_optimal(i)=min([taux_monte_optimal_thrust(i) taux_monte_optimal_power(i)]);
          % Power requis pour cette mont�
        Power_requis(i)=spline(P.taux_monte,P.Puissance_kW.monter,taux_monte_optimal(i));
    end
 
        


%% Solution trouv�
[taux_monte_optimal,i]=max(taux_monte_optimal);
vitesse_avance_monte_optimal=V_avance_en_monter(i);
   case 'off'
       taux_monte_optimal=P.taux_monte;
       vitesse_avance_monte_optimal=P.V_monte_takeoff;
end


%% V�rification graphique de la m�thode
% yyaxis('left');
% hold on
% plot(convvel(V_monter,'m/s','mph'),convvel(taux_monte_optimal,'m/s','ft/min'))
% xlabel('Vitesse avance (mph)'),ylabel(['Vitesse mont� (ft/min) pour T= ' num2str(P.Thrust_max_N) ' N '])
% 
% yyaxis('right')
% plot(convvel(V_monter,'m/s','mph'),Power_requis)
% ylabel('Power requis (kW)')

end

