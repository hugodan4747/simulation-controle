%% Simulation compl�te d'une mission
clear all;

%% Param�te de l'avion  (different cas selon les concepts choisis)
cas=1
switch cas
    case 1
%% KR2 HERA �lectrique  cas 8P (d�but session S7) 
P.name='KR2';
P.e=convlength(20.8,'ft','m');         % m      Envergure
P.S=78*convlength(1,'ft','m')^2;       % m^2    Surface
P.c=P.S/P.e;                           % m      Corde

P.m.empty=200;                         % kg     Masse vide de l'avion (pas de moteur)
P.m.engine=NaN;                        % kg     masse moteur
P.AR=P.e/P.c;                          % --     Aspect ratio
P.n_batterie=0.95^2;                     % --     Rendement batterie
P.n_helice_take_off=0.75;              % --     Rendement h�lice take off
P.n_helice=0.75;                       % --     Rendement h�lice
P.Cd0=0.0220;
P.Cd0_additionnel=0.0*0.1542/P.S;      % --     Coefficient de drag parasite reli� � l'ajout de composante externe
P.Cd0=P.Cd0+P.Cd0_additionnel;         % --     Coefficient de drag parasite totale
P.puissance_moteur_max=100e3;  % w     Puissance disponible du moteur 
P.Power_decollage_w=P.puissance_moteur_max;                  % w     Puissance max au d�collage

P.Thrust_max_N=1520; % N Force max consid�r� pour l'h�lice


%[Energie_finale,masse_tot_apres] = energie_vs_masse(1,22)

P.m.gross=convmass(1110,'lbm','kg');   % kg     Masse maximal
P.Energie_kWh.total=21.8;

    case 2
%% KR2 HERA �lectrique moins de cellules (7P)
P.name='KR2';
P.e=convlength(20.8,'ft','m');         % m      Envergure
P.S=78*convlength(1,'ft','m')^2;       % m^2    Surface
P.c=P.S/P.e;                           % m      Corde

P.m.empty=200;                         % kg     Masse vide de l'avion (pas de moteur)
P.m.engine=NaN;                        % kg     masse moteur
P.AR=P.e/P.c;                          % --     Aspect ratio
P.n_batterie=0.95^2;                      % --     Rendement batterie
P.n_helice_take_off=0.75;               % --     Rendement h�lice take off
P.n_helice=0.75;                       % --     Rendement h�lice
P.Cd0=0.0220;
P.Cd0_additionnel=0.0*0.1542/P.S;     % --     Coefficient de drag parasite reli� � l'ajout de composante externe
P.Cd0=P.Cd0+P.Cd0_additionnel;         % --     Coefficient de drag parasite totale
P.puissance_moteur_max=87e3;  % w     Puissance disponible du moteur 
P.Power_decollage_w=P.puissance_moteur_max;                  % w     Puissance max au d�collage

P.Thrust_max_N=1324; % N Force max consid�r� pour l'h�lice
P.m.gross=convmass(1063.3,'lbm','kg');   % kg     Masse maximal
P.Energie_kWh.total=21.8*7/8;

    case 3
%% KR2 HERA �lectrique moins de cellules (6P)
P.name='KR2';
P.e=convlength(20.8,'ft','m');         % m      Envergure
P.S=78*convlength(1,'ft','m')^2;       % m^2    Surface
P.c=P.S/P.e;                           % m      Corde

P.m.empty=200;                         % kg     Masse vide de l'avion (pas de moteur)
P.m.engine=NaN;                        % kg     masse moteur
P.AR=P.e/P.c;                          % --     Aspect ratio
P.n_batterie=0.95^2;                      % --     Rendement batterie
P.n_helice_take_off=0.75;               % --     Rendement h�lice take off
P.n_helice=0.75;                       % --     Rendement h�lice
P.Cd0=0.0220;
P.Cd0_additionnel=0.0*0.1542/P.S;     % --     Coefficient de drag parasite reli� � l'ajout de composante externe
P.Cd0=P.Cd0+P.Cd0_additionnel;         % --     Coefficient de drag parasite totale
P.puissance_moteur_max=74.7e3;  % w     Puissance disponible du moteur 
P.Power_decollage_w=P.puissance_moteur_max;                  % w     Puissance max au d�collage

P.Thrust_max_N=1147; % N Force max consid�r� pour l'h�lice
P.m.gross=convmass(1036.7,'lbm','kg');   % kg     Masse maximal
P.Energie_kWh.total=21.8*6/8;


    case 4
        % KR2 avant modification
P.name='KR2-avant';
P.e=convlength(20.8,'ft','m');         % m      Envergure
P.S=78*convlength(1,'ft','m')^2;       % m^2    Surface
P.c=P.S/P.e;                           % m      Corde

P.m.gross=convmass(1050,'lbm','kg');   % kg     Masse maximal
P.m.empty=200;                         % kg     Masse vide de l'avion (pas de moteur)
P.m.engine=NaN;                        % kg     masse moteur
P.AR=P.e/P.c;                          % --     Aspect ratio
P.n_batterie=1;                      % --     Rendement batterie
P.n_helice_take_off=0.65;               % --     Rendement h�lice take off
P.n_helice=0.75;                       % --     Rendement h�lice
P.Cd0=0.0220;
P.Cd0_additionnel=0.0*0.1542/P.S;     % --     Coefficient de drag parasite reli� � l'ajout de composante externe
P.Cd0=P.Cd0+P.Cd0_additionnel;         % --     Coefficient de drag parasite totale
P.puissance_moteur_max=85*745.699872;  % w     Puissance disponible du moteur 
P.Power_decollage_w=85*745.699872;                  % w     Puissance max au d�collage
P.Energie_kWh.total=200;
P.Thrust_max_N=1236; % N Force max consid�r� pour l'h�lice
end
%% Parametre de la mission
P.rho_0=1.225;                                      % kg/m^3 Masse volumique de l'air au sol
P.delta_t.pretakeoff=30;                            % sec Temps avant le d�collage d�sir�
P.V_monte_takeoff = convvel(97,'mph','m/s');       % m/s vitesse d'avance
P.taux_monte= convvel(1900,'ft/min','m/s');         % m/s taux de mont�e
P.Altitude_cruise = convlength(1000,'ft','m');      % m Hauteur de vol en cruise
P.v_cruise= convvel(140,'mph','m/s');               % m/s vitesse lin�aire en mont�
P.g=9.81;                                           % m/s^2 Constante gravitationnelle
% Param�tre de calcul interm�diaires
P.K_CL=(pi*P.AR*0.85)^-1;
P.poids=P.m.gross*P.g;                                 % N Poids de l'avion 
P.Cl_max=1.5;                                          % - Limite de coefficient de lift avant d�crochage
P.V_stall_sol=(P.poids/0.5/P.Cl_max/P.rho_0/P.S)^0.5;  % - Vitesse de d�crochage

% Option d'optimisation de la vitesse de vol au point de finesse maximale de l'avion
P.vitesse_optimise='on';                               % vitesse de vol au d�collage et en cruise
[P.taux_monte,P.V_monte_takeoff] = optimisation_taux_monter(P, 90:0.1:97, 1000:3000); % Code optimisation du taux de mont�
%% Afficahge de param�tres
% convvel(P.V_monte_takeoff,'m/s','mph') ; 
convvel(P.taux_monte,'m/s','ft/min') 
convvel(5.06,'m/s','ft/min');
% convvel(P.V_stall_sol,'m/s','mph') ;
%% Estimation de la distance de d�collage au sol
P.mu=0.08;
Vit=linspace(0,P.V_stall_sol*1.1);
CL=0.10
a=P.g*((P.Thrust_max_N/P.poids-P.mu)+(P.rho_0/(2*P.poids/P.S)).*(-P.Cd0-P.K_CL*CL.^2+P.mu.*CL).*Vit.^2)
distance=diff(Vit.^2)./(2*mean([a(1:end-1);a(2:end)]))
distance=sum(distance)
convlength(distance,'m','ft')

%% Simulation de la mission - performance de vol 
[P] = simulation_mission(P);
%% Plotting graphique
figure(cas)
c = categorical({'1 Pre-takeoff','2 take-off','3 Cruise'});
color_bar=[0.5 0.7 0.2];

n=1;SUBPLOT(n)=subplot(4,1,n);


bar(c,[P.thrust.monter P.thrust.monter P.thrust.cruise],'Facecolor',color_bar)

text(1, mean(SUBPLOT(n).YLim), num2str(P.thrust.monter),'HorizontalAlignment','center','backgroundcolor','w');
text(2, mean(SUBPLOT(n).YLim), num2str(P.thrust.monter),'HorizontalAlignment','center','backgroundcolor','w');
text(3, mean(SUBPLOT(n).YLim), num2str(P.thrust.cruise),'HorizontalAlignment','center','backgroundcolor','w');
ylabel('Thrust (N)')


n=2;SUBPLOT(n)=subplot(4,1,n);

bar(c,[P.Puissance_kW.pretakeoff P.Puissance_kW.monter P.Puissance_kW.cruise],'Facecolor',color_bar)
text(1, mean(SUBPLOT(n).YLim), num2str(P.Puissance_kW.pretakeoff),'HorizontalAlignment','center','backgroundcolor','w');
text(2, mean(SUBPLOT(n).YLim), num2str(P.Puissance_kW.monter),'HorizontalAlignment','center','backgroundcolor','w');
text(3, mean(SUBPLOT(n).YLim), num2str(P.Puissance_kW.cruise),'HorizontalAlignment','center','backgroundcolor','w');
ylabel('Puissance (kW)');


n=3;SUBPLOT(n)=subplot(4,1,n);
bar(c,[P.Energie_kWh.pretakeoff P.Energie_kWh.monter P.Energie_kWh.cruise],'Facecolor',color_bar)
text(1, mean(SUBPLOT(n).YLim), num2str(P.Energie_kWh.pretakeoff),'HorizontalAlignment','center','backgroundcolor','w');
text(2, mean(SUBPLOT(n).YLim), num2str(P.Energie_kWh.monter),'HorizontalAlignment','center','backgroundcolor','w');
text(3, mean(SUBPLOT(n).YLim), num2str(P.Energie_kWh.cruise),'HorizontalAlignment','center','backgroundcolor','w');
ylabel('Ener. requ. (kWh)')

n=4;SUBPLOT(n)=subplot(4,1,n);
bar(c,[P.delta_t.pretakeoff P.delta_t.monter P.delta_t.cruise]/60,'Facecolor',color_bar)
esp=0;
text(1, mean(SUBPLOT(n).YLim), num2str(P.delta_t.pretakeoff/60),'HorizontalAlignment','center','backgroundcolor','w');
text(2, mean(SUBPLOT(n).YLim), num2str(P.delta_t.monter/60),'HorizontalAlignment','center','backgroundcolor','w');
text(3, mean(SUBPLOT(n).YLim), num2str(P.delta_t.cruise/60),'HorizontalAlignment','center','backgroundcolor','w');
ylabel('Temps (min)')

subplot(4,1,1);
title(['Perfomances : GW = ' num2str(convmass(P.m.gross,'kg','lbm')) ' lbs -  Taux mont� = ' num2str(convvel(P.taux_monte,'m/s','ft/min')) ' @ ' num2str(convvel(P.V_monte_takeoff,'m/s','mph')) 'mph' ]);

%% Estimation des performances selon la masse des batteries
 % --------------------------Non applicable--------------------------------
% clear Masse Energie temps_cruise_min taux_monter_ft_min
% proportion=linspace(0.75,1,15);
% for i=1:length(proportion)
%     
%     [Energie_finale,masse_tot_apres,Power_total_apres] = energie_vs_masse(proportion(i),22,118000);
% P.m.gross=convmass(masse_tot_apres,'lbm','kg');   % kg     Masse maximal
% P.Energie_kWh.total=Energie_finale;
% P.puissance_moteur_max=Power_total_apres;
% 
% P.vitesse_optimise='on';% vitesse de vol au d�collage et en cruise optimisable
% [P.taux_monte,P.V_monte_takeoff] = optimisation_taux_monter(P, 70:0.1:96, 1000:3000);
% [P] = simulation_mission(P);
% 
% 
% Masse(i)=convmass(P.m.gross,'kg','lbm');
% Energie(i)=P.Energie_kWh.cruise;
% temps_cruise_min(i)=P.delta_t.cruise/60;
% taux_monter_ft_min(i)=convvel(P.taux_monte,'m/s','ft/min');
% vitesse_TAS(i)=convvel(P.V_monte_takeoff,'m/s','mph');
% end
% %%
% 
% figure('Name','Variation spec batteries')
% subplot(5,1,1)
% plot(proportion,22*proportion)
% xlabel('Proportion Energie');ylabel('Energie total (kW)')
% 
% subplot(5,1,2)
% plot(proportion,Masse)
% xlabel('Proportion Energie');ylabel('Masse')
% 
% subplot(5,1,3)
% plot(proportion,temps_cruise_min)
% xlabel('Proportion Energie');ylabel('Temps cruise (min)')
% 
% subplot(5,1,4)
% plot(proportion,taux_monter_ft_min)
% xlabel('Proportion Energie');ylabel('Taux de mont� (ft/min)')
% 
% subplot(5,1,5)
% plot(proportion,Energie)
% xlabel('Proportion Energie');ylabel('Energie en cruise (kW)')
% 
% 
