%% Analysis of propeller performances based on data from manufacturer
%
%Projet HERA
%Last modified on : 16/01/2019
%By Bruno Parent
%% Inputs
RPM_max = 3500; %rpm
RPM_min = 1000; %rpm
delta_rpm = 50;
rho_0 = 0.002377; %slug/ft�, air density at sea level
rho_2000 = 0.002245; %slug/ft�, air density at 2000 ft
rho_5000 = 0.002048; %slug/ft�, air density at 5000 ft
rho = rho_0;
sound_speed = 1125; %ft/sec

%% Contraints
power_max_batt = 100; %kW
total_energy = 21.8; %kWh, battery capacity
thrust_min_TO = 1400; %N

voltage_max = 453.4; %Volts
kV_emrax = 7; %full load (8.2 for no load)
current_max = power_max_batt*1000/voltage_max;
eff_drive = 0.95; 
eff_emrax = 0.95;
tip_speed_max = 0.8; %Mach
torque_peak = current_max*1.4; %Nm 1.4*Arms
torque_cont = 250; %Nm
rpm_max = voltage_max*kV_emrax; %rpm
thrust_min_cruise = 350; %N
thrust_max_cruise = 450; %N
power_max_TO = power_max_batt*eff_drive*eff_emrax; %kW
power_max_cruise = 35; %kW
power_min_cruise = 10; %kW
TAS_min = 95; %mph
TAS_max = 105; %mph
VNE = 180; %mph
%% Load propeller performance txt file
% Columns order : J(1)    eta(2)     CT(3)     CP(4)
data = load(file);
J = data(:,1);  %initialization of advance ratio
eta = data(:,2);  %initialization of efficiency
CT = data(:,3);  %initialization of thrust coefficient
CP = data(:,4);  %initialization of power coefficient
%% Calculation of performances
%rpm of a column = column number*delta_rpm+RPM_min-delta_rpm
i = 1;
TAS = zeros(length(J),(RPM_max-RPM_min)/delta_rpm+1);
T = zeros(length(J),(RPM_max-RPM_min)/delta_rpm+1);
P = zeros(length(J),(RPM_max-RPM_min)/delta_rpm+1);
C = zeros(length(J),(RPM_max-RPM_min)/delta_rpm+1);
for rpm = RPM_min:delta_rpm:RPM_max
    rps = rpm/60;
    TAS(:,i) = J*rps*dia*0.681818; %mph (ft/s -> mph = 0.681818)
    T(:,i) = CT*rho*rps^2*dia^4*4.44822; %thrust, Newtons (lb -> Newton = 4.44822)
    P(:,i) = CP*rho*rps^3*dia^5*0.001818*0.7457; %power, kW (lb*ft/sec -> hp -> kW)   
    C(:,i) = P(:,i)*1000/(rpm*2*pi/60);  %torque, Nm
    
    % Emrax 268 performances from data sheet
    p_mot(i)=0.02*rpm;%continuous power function of RPM(kW)
    p_mot_peak(i)=0.0575*rpm;%peak power function of RPM(kW)
    c_mot(i)=-0.000015*(rpm^2)+0.06*rpm+190;%continuous torque function of RPM (Nm)
    c_mot_peak(i)=-0.00000625*(rpm^2)+0.0125*rpm+500;%peak torque function of RPM (Nm)
    
    i = i+1;
end

%% Determine operating points in climb
fprintf('Here are the potential climb operating points for a %0.0f� pitch:\n\n', pitch)
fprintf([' #     RPM    TAS(mph)   Tip (Mach)      eta      Thrust(N)   Power(kW)   Torque(Nm)\n' ...
         '---   -----   --------   ----------   ---------   ---------   ---------   ----------\n']);
power_test_TO = 99999;
num = 0;
num_best_TO = 0;
for i = 1:length(data)
    for j = 1:((RPM_max-RPM_min)/delta_rpm+1)
        if T(i,j) > thrust_min_TO 
            if P(i,j) < power_max_TO  
                if C(i,j) < torque_peak 
                    if TAS(i,j) > TAS_min && TAS(i,j) < TAS_max %between the airspeed range
                        tip_speed = ((j*delta_rpm+RPM_min-delta_rpm)/60*2*pi*(dia/2))/sound_speed;
                        if tip_speed < tip_speed_max % < Mach 0.8
                            rpm = j*delta_rpm+RPM_min-delta_rpm;
                            if rpm < rpm_max % battery constraint
                                num = num + 1;
                                fprintf('%3.0f   %5.0f   %8.4f   %10.4f   %9.4f   %9.4f   %9.4f   %10.4f\n', num, rpm, TAS(i,j), tip_speed, eta(i), T(i,j), P(i,j), C(i,j))
                                plot_rpm_TO(num) = rpm;
                                plot_power_TO(num) = P(i,j);
                                plot_torque_TO(num) = C(i,j);
                                if P(i,j) < power_test_TO %used to find optimum
                                    power_test_TO = P(i,j);
                                    num_best_TO = num;
                                end
                            end
                        end                                       
                    end
                end
            end
        end
    end      
end
if num_best_TO ~= 0 
    fprintf('\nBest operating point in climb is #%1.0f for shaft power = %5.4f kW\n\n',num_best_TO, power_test_TO);
else
    fprintf('This propeller configuration doesn''t have the required performances for the climb.\n\n')    
end
%% Determine operating points in cruise
fprintf('Here are the potential cruise operating points for a %0.0f� pitch:\n\n',pitch)
fprintf([' #     RPM    TAS(mph)   Tip (Mach)      eta      Thrust(N)   Power(kW)   Torque(Nm)\n' ...
         '---   -----   --------   ----------   ---------   ---------   ---------   ----------\n']);
power_test_cruise = 99999;
num = 0;
num_best_cruise = 0;
for i = 1:length(data)
    for j = 1:((RPM_max-RPM_min)/delta_rpm+1)
        if T(i,j) > thrust_min_cruise && T(i,j) < thrust_max_cruise %thrust validation
            if P(i,j) < power_max_cruise && P(i,j) > power_min_cruise    %power validation
                if C(i,j) < torque_cont %torque < 250 Nm
                    if TAS(i,j) > TAS_min && TAS(i,j) < TAS_max %between the airspeed range
                        tip_speed = ((j*delta_rpm+RPM_min-delta_rpm)/60*2*pi*(dia/2))/sound_speed;
                        if tip_speed < tip_speed_max %< Mach 0.8
                            rpm = j*delta_rpm+RPM_min-delta_rpm;
                            if rpm < rpm_max %battery contraint
                                num = num + 1;
                                fprintf('%3.0f   %5.0f   %8.4f   %10.4f   %9.4f   %9.4f   %9.4f   %10.4f\n', num, rpm, TAS(i,j), tip_speed, eta(i), T(i,j), P(i,j), C(i,j))
                                plot_rpm_cruise(num) = rpm; %#ok<*SAGROW>
                                plot_power_cruise(num) = P(i,j);
                                plot_torque_cruise(num) = C(i,j);
                                if P(i, j) < power_test_cruise
                                    power_test_cruise = P(i,j);
                                    TAS_cruise = TAS(i,j);
                                    num_best_cruise = num;
                                end
                            end
                        end                                       
                    end
                end
            end
        end
    end      
end
if num_best_cruise ~= 0
    fprintf('\nBest operating point in cruise is #%1.0f for shaft power = %5.4f kW\n\n',num_best_cruise, power_test_cruise);
else
    fprintf('This propeller configuration doesn''t have the required performances for the cruise.\n\n') 
end  
%% Flight autonomy estimation
%Flight mission : climb during 2 min, 30 sec linear transition and cruise for the rest of the flight
time_cruise = 0;
if num_best_TO ~= 0 && num_best_cruise ~= 0
    energy_climb = power_test_TO/(eff_drive/eff_emrax)*2/60; 
    %Linear transition from climb to cruise
        power_transition = linspace(power_test_TO, power_test_cruise, 31);
        energy_transition = sum(power_transition(:)*1/3600); %seconds to hour to calculate kWh 
    
    energy_cruise = (total_energy-energy_climb-energy_transition);
    time_cruise = energy_cruise/(power_test_cruise/eff_drive/eff_emrax)*60;
end
%% Plotting results
fontsize = 20;
if num_best_TO ~= 0 && num_best_cruise ~= 0
    rpm = RPM_min:delta_rpm:RPM_max;
    figure();
    [x, power_y, torque_y] = plotyy([rpm', rpm'], [p_mot', p_mot_peak'],[rpm', rpm'], [c_mot', c_mot_peak']); %%Continuous and peak power and torque
    power_y(2).Color = 'b';
    power_y(1).Color = 'b';
    torque_y(2).Color = 'r';
    torque_y(1).Color = 'r';
    power_y(1).LineStyle = '--';
    torque_y(1).LineStyle = '--';
    hold(x(1),'on'); %'on' supresses the unnecessary Matlab output
    hold(x(2),'on');
    plot(x(1),plot_rpm_TO,plot_power_TO,'xb', 'LineWidth', 8); %TO power operating points
    plot(x(1),plot_rpm_cruise,plot_power_cruise,'ob', 'LineWidth',8); %Cruise power operating points
    plot(x(2),plot_rpm_TO,plot_torque_TO,'xr');  %TO Torque operating points
    plot(x(2),plot_rpm_cruise,plot_torque_cruise,'or'); %Cruise Torque operating points
    lgd = legend('Continuous power','Peak Power','Prop TO power','Prop Cruise power','Continuous torque','Peak Torque','Prop TO Torque','Prop Cruise Torque','Location','northeast');
    lgd.FontSize = fontsize;
    title(strcat('Propeller (pitch = ',num2str(pitch) ,'�) and EMRAX 268 performances'),'FontSize', 24);
    xlabel('RPM','FontSize', fontsize);
    ylabel(x(1),'Power (kW)','FontSize', fontsize);
    ylabel(x(2),'Torque (Nm)','FontSize', fontsize);
    
%     %RCD
%     figure()
%     hold on
%     plot(rpm, p_mot, 'r', 'LineWidth', 4)
%     plot(rpm, p_mot_peak, 'b', 'LineWidth', 4)
%     plot(plot_rpm_TO,plot_power_TO,'xb', 'LineWidth', 4)
%     plot(plot_rpm_cruise,plot_power_cruise,'xr', 'LineWidth', 4)
%     set(gca,'FontSize', fontsize)
%     title(strcat('Propeller (pitch = ',num2str(pitch) ,'�) and EMRAX 268 performances'),'FontSize', 24);
%     xlabel('RPM','FontSize', fontsize);
%     ylabel('Power (kW)','FontSize', fontsize);
%     ldg = legend('Continuous power','Peak Power','Prop TO power','Prop Cruise power','Location','northeast');
%     lgd.FontSize = fontsize;
    
    %Surfaces tests
%     [m,n] = size(C);    
%     torque_matrix = cell(m,n); 
%     thrust_matrix = cell(m,n); 
%     power_matrix = cell(m,n); 
%     for i = 1:m
%         for j = 1:n
%             torque_matrix{i,j} = [j*delta_rpm+RPM_min-delta_rpm, TAS(i,j), C(i,j)]; % [rpm, TAS, Torque]
%             thrust_matrix{i,j} = [j*delta_rpm+RPM_min-delta_rpm, TAS(i,j), T(i,j)]; % [rpm, TAS, Thrust]
%             power_matrix{i,j} = [j*delta_rpm+RPM_min-delta_rpm, TAS(i,j), P(i,j)]; % [rpm, TAS, Power]
%         end
%     end
% 
%     figure()
%     for i = 1:m
%         for j = 1:n
%             plot3(torque_matrix{i,j}(1),torque_matrix{i,j}(2),torque_matrix{i,j}(3),'ob')
%             hold on;
%         end
%     end
%     surf(rpm, TAS, C)
%     xlabel('RPM')
%     ylabel('TAS')
%     zlabel('Torque (Nm)')
% 
%     figure()
%     for i = 1:m
%         for j = 1:n
%             plot3(thrust_matrix{i,j}(1),thrust_matrix{i,j}(2),thrust_matrix{i,j}(3),'ob')
%             hold on;
%         end
%     end
%     surf(rpm, TAS, T)
%     xlabel('RPM')
%     ylabel('TAS')
%     zlabel('Thrust (N)')
%     
%     figure()
%     for i = 1:m
%         for j = 1:n
%             plot3(power_matrix{i,j}(1),power_matrix{i,j}(2),power_matrix{i,j}(3),'ob')
%             hold on;
%         end
%     end
%     surf(rpm, TAS, P)
%     xlabel('RPM')
%     ylabel('TAS')
%     zlabel('Power (kW)')
    

    %Surfaces
    figure()
    surf(rpm, TAS, C)
    title(strcat('Torque VS RPM and TAS (pitch = ',num2str(pitch) ,'�)'))
    zlabel('Torque (Nm)')
    ylabel('TAS (mph)')
    xlabel('RPM')
    figure()
    surf(rpm, TAS, T)
    title(strcat('Thrust VS RPM and TAS (pitch = ',num2str(pitch) ,'�)'))
    zlabel('Thrust (N)')
    ylabel('TAS (mph)')
    xlabel('RPM')
    figure()
    surf(rpm, TAS, P)
    title(strcat('Power VS RPM and TAS (pitch = ',num2str(pitch) ,'�)'))
    zlabel('Power (kW)')
    ylabel('TAS (mph)')
    xlabel('RPM')
    
    %Thrust reduction function of TAS
    figure(); hold on
    for i = 1:(250/delta_rpm):length(TAS) %bon de 500 rpm
        plot(TAS(:,i), T(:,i));
    end
    legend('1000 RPM','1250 RPM','1500 RPM','1750 RPM','2000 RPM','2250 RPM','2500 RPM','2750 RPM','3000 RPM','3250 RPM','3500 RPM')
    xlabel('TAS (mph)');
    ylabel('Thrust (N)')
    title(strcat('Thrust reduction function of TAS for multiple RPM (pitch = ',num2str(pitch) ,'�)'))
end

%Setting variables to 0
plot_rpm_TO = 0; 
plot_power_TO = 0;
plot_torque_TO = 0;
plot_rpm_cruise = 0; 
plot_power_cruise = 0;
plot_torque_cruise = 0;



