close all
clear all
clc

C1 = table2array(readtable('1C.csv')) ;
A5 = table2array(readtable('5A.csv')) ;
A10 = table2array(readtable('10A.csv')) ;
A15 = table2array(readtable('15A.csv')) ;
A20 = table2array(readtable('20A.csv')) ; 

%% Approximation de l'�quation exponentiel
f1C = fit(C1(:,1),C1(:,2),'exp2');
f5A = fit(A5(:,1),A5(:,2),'exp2');
f10A = fit(A10(:,1),A10(:,2),'exp2');
f15A = fit(A15(:,1),A15(:,2),'exp2');
f20A = fit(A20(:,1),A20(:,2),'exp2');

% figure;
% plot(f1C,C1(:,1),C1(:,2));
% figure;
% plot(f5A,A5(:,1),A5(:,2));
% figure;
% plot(f10A,A10(:,1),A10(:,2));
% figure;
% plot(f15A,A15(:,1),A15(:,2));
% figure;
% plot(f20A,A20(:,1),A20(:,2));

%% Resistance
% R = dV/dI
x = 0:0.0001:0.1;

V_R1C = feval(f1C,x);
V_R5A = feval(f5A,x);
V_R10A = feval(f10A,x);
V_R15A = feval(f15A,x);
V_R20A = feval(f20A,x);

R1C = abs(diff(V_R1C)./diff(x'));
R5A = abs(diff(V_R5A)./diff(x'));
R10A = abs(diff(V_R10A)./diff(x'));
R15A = abs(diff(V_R15A)./diff(x'));
R20A = abs(diff(V_R20A)./diff(x'));

figure;
plot(x(1,2:end), R1C)
title("Graphique de l'estimation de la r�sistance interne � un courand de d�charge de 1C (batterie IRQ18650)");
xlabel("Capacit� (Ah)")
ylabel("R�sistance interne (\Omega)");
figure;
plot(x(1,2:end), R5A)
title("Graphique de l'estimation de la r�sistance interne � un courand de d�charge de 5A (batterie IRQ18650)");
xlabel("Capacit� (Ah)")
ylabel("R�sistance interne (\Omega)");
figure;
plot(x(1,2:end), R10A)
title("Graphique de l'estimation de la r�sistance interne � un courand de d�charge de 10A (batterie IRQ18650)");
xlabel("Capacit� (Ah)")
ylabel("R�sistance interne (\Omega)");
figure;
plot(x(1,2:end), R15A)
title("Graphique de l'estimation de la r�sistance interne � un courand de d�charge de 15A (batterie IRQ18650)");
xlabel("Capacit� (Ah)")
ylabel("R�sistance interne (\Omega)");
figure;
plot(x(1,2:end), R20A)
title("Graphique de l'estimation de la r�sistance interne � un courand de d�charge de 20A (batterie IRQ18650)");
xlabel("Capacit� (Ah)")
ylabel("R�sistance interne (\Omega)");








